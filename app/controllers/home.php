<?php
	
	class Home extends Controller{
		protected $model;
		protected $view;
		
		function __construct($params){
			parent::__construct($params);
			$this->model = new mHome();
			$this->view = new vHome();
		}
		function home(){
			
		}
		//Función para redireccionar desde el inicio, recibe y devuelve hacia el ajax
		function redireccionar(){

			$x = $_POST['id'];
			
			if($x=="l_nene")
			{
                $this->json_out(array('redirect' => APP_W.'desktop'));
			}
			else
			{
                $this->json_out(array('redirect'=>APP_W.'parents'));
			}
		}
		function iniciar_sesion(){
			if (isset($_POST['username']) && isset($_POST['password'])) {
            	$username = filter_input(INPUT_POST, 'username', FILTER_SANITIZE_STRING);
            	$password = filter_input(INPUT_POST, 'password', FILTER_SANITIZE_STRING);
            	$user = $this->model->iniciar_sesion($username, $password);
	            if ($user == TRUE) {
	            	Session::set('user', $username);
	            	if($user == 1){
	            		$this->json_out(array('redirect' => APP_W.'desktop'));
	            	}elseif($user == 2){
	            		$this->json_out(array('redirect' => APP_W.'parents'));
	            	}elseif($user == 3){
	            		$this->json_out(array('redirect' => APP_W.'dashboard'));
	            	}
	            } else {
	            	$this->json_out(array('redirect' => APP_W.'error'));
	            }
	        }
		}
		function cerrar_sesion(){
			Session::destroy();
			$this->json_out(array('redirect' => APP_W));
		}
}