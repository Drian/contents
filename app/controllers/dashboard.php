<?php
	
	class Dashboard extends Controller{
		protected $model;
		protected $view;
		
		function __construct($params){
			parent::__construct($params);
			$this->model = new mDashboard();
			$this->view = new vDashboard();
		}
		function home(){
			
		}
		function registrar_user(){
			if((!empty($_POST['nickname'])) && (!empty($_POST['password'])) && (!empty($_POST['nombre'])) && (!empty($_POST['apellidos'])))
			{
				$tipo=filter_input(INPUT_POST, 'tipo', FILTER_SANITIZE_STRING);
				$nickname=filter_input(INPUT_POST, 'nickname', FILTER_SANITIZE_STRING);
				$mail=filter_input(INPUT_POST, 'mail', FILTER_SANITIZE_STRING);
				$nif=filter_input(INPUT_POST, 'nif', FILTER_SANITIZE_STRING);
				$nombre=filter_input(INPUT_POST, 'nombre', FILTER_SANITIZE_STRING);
				$apellidos=filter_input(INPUT_POST, 'apellidos', FILTER_SANITIZE_STRING);
				$telefono=filter_input(INPUT_POST, 'telefono', FILTER_SANITIZE_STRING);
				$password=filter_input(INPUT_POST, 'password', FILTER_SANITIZE_STRING);
				$papa=filter_input(INPUT_POST, 'papa', FILTER_SANITIZE_STRING);

				$regUser=$this->model->registrar_user($tipo, $nick, $mail, $nif, $nombre, $apellidos, $telefono, $password, $papa);

				if($regUser==true)
				{
					//Registro completado
					//$this->ajax_out(array('redirect'=>APP_W.'dashboard'));
					//$output=array('redirect'=>APP_W);
             	 	$this->ajax_set(1);
				}
				else
				{
					//No se ha podido registrar
					/*$output=array('redirect'=>APP_W.'dashboard');
             	 	$this->ajax_set($output);*/
             	 	$this->ajax_set(2);
				}
			}
		}
}