<?php

	class mHome extends Model{

		function __construct(){
			parent::__construct();
			
		}
		function iniciar_sesion($username, $password) {
	        try {

	            $sql = "SELECT * FROM usuarios WHERE nickname=? AND password=?";
	            $this->query($sql);
	            $this->bind(1, $username);
	            $this->bind(2, $password);
	            $this->execute();

	            $user = $this->single();

	            if ($this->rowCount() == 1)
	            {
	                if ($user[0]['rol'] == 1) {
	                    return 1;
	                } else {
	                    return 2;
	                }
	            }
	            else
	            {
	                return FALSE;
	            }

	        } catch (PDOException $e) {
	            echo "Error:" . $e->getMessage();
	        }
    	}

	}