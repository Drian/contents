<?php

	class mRegister extends Model{

		function __construct(){
			parent::__construct();
			
		}
		function registrar($username, $name, $lastname, $email, $password, $phonenumber, $nif, $rol){
			try {

				$sql = "SELECT * FROM usuarios WHERE nickname=? OR email=? OR nif=?";
	            $this->query($sql);
	            $this->bind(1, $username);
	            $this->bind(2, $email);
	            $this->bind(3, $nif);
	            $this->execute();
	            if ($this->rowCount() == 0) {
	            	$sql = "INSERT INTO usuarios(nickname, nombre, apellidos, email, password, telefono, nif, rol) VALUES (?,?,?,?,?,?,?,?)";
	            	$this->query($sql);
	            	$this->bind(1, $username);
	            	$this->bind(2, $name);
	            	$this->bind(3, $lastname);
	            	$this->bind(4, $email);
	            	$this->bind(5, $password);
	            	$this->bind(6, $phonenumber);
	            	$this->bind(7, $nif);
	            	$this->bind(8, $rol);
	            	$this->execute();
	            	return TRUE;
	            }else{
	            	return FALSE;
	            }
			} catch (PDOException $e) {
	            echo "Error:" . $e->getMessage();
	        }
		}
}