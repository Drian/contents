<div class="container3">
	<div id="lc">
		<h3>Listado de cuentos</h3>
	</div>
		
	<div id="ac">
		<h3>Agregar Contenido</h3>
		<select id="categ_nc">
			<option value="">- Selecciona -</option>
			<option value="1">Nivel inicial</option>
			<option value="2">Nivel intermedio</option>
			<option value="3">Nivel dificil</option>
		</select>
		<input type="text" name="titulo" placeholder="titulo"><br>
		<input type="button" id="npag" value="+">
	</div>

	<div id="au">
		<h3>Administración de usuarios</h3>
		<div id="mu">
			<p>Aquí va la tabla de usuarios para el admin</p>
		</div>
		<div id="nu">
			<form class="nu" method="POST" name="register" action="<?=APP_W.'dashboard/registrar_user'; ?>">
				<label>Tipo de usuario</label>
				<select id="tipo" name="tipo">
					<option value="">- Selecciona -</option>
					<option value="1">Padre/Madre</option>
					<option value="2">Niño/a</option>
					<option value="3">Profesor/a</option>
				</select><br>
				<label class="gen" id="li1">Alias</label><input id="input1" class="gen" type="text" name="nickname" placeholder="AdolfHittler95"><br>
				<label id="li2">Em@il</label><input id="input2" class="padre" type="email" name="mail" placeholder="david_star_wins@outlook.com"><br>
				<label id="li3">NIF</label><input id="input3" class="padre" type="text" name="nif" placeholder="98989877-T"><br>
				<label class="gen" id="li4">Contraseña</label><input id="input4" class="gen" type="password" name="password" placeholder="**********"><br>
				<label class="gen" id="li5">Nombre</label><input id="input5" class="gen" type="text" name="nombre" placeholder="Adolf"><br>
				<label class="gen" id="li6">Apellidos</label><input id="input6" class="gen" type="text" name="apellidos" placeholder="Hittler"><br>
				<label id="li7">Telefono</label><input id="input7" class="padre" type="text" name="telefono" placeholder="936658484"><br>
				<label class="gen" id="li8">Padre(NIF)</label><input id="input8" class="gen" type="text" name="papa" placeholder="78652144-T"><br>
				<input type="Submit" class="gen" id="input9" value="Registrar"><br>
			</form>
		</div>
	</div>
</div>