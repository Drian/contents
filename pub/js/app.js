$(document).ready(function(){

	$("#li_desplegable").hide();
	var cont=1;

	//Para desplegar el formulario de login
	$("#login").click(function(){
		
		cont++;
		if(cont%2==0)
		{
			$("#li_desplegable").slideDown();
		}
		else
		{
			$("#li_desplegable").slideUp();
		}
	});

	//Redireccionar desde la pagina de inicio hacia niños o padres
	$("#l_nene").click(function(){
		var x=$(this).attr("id");
	});

	$('#form_cerrar_sesion').on('submit', function () {
        var postData = $(this).serialize();
        var formURL = $(this).attr("action");
        $.ajax({
            url: formURL,
            type: 'post',
            data: postData,
            dataType: 'json',
            error: function () {
                alert('Error al cerrar sesion');
            },
            success: function (resp) {
                window.location.href = resp.redirect;
            }
        });
    });

	$('#form_login').on('submit', function () {
        var postData = $(this).serialize();
        var formURL = $(this).attr("action");
        $.ajax({
            url: formURL,
            type: 'post',
            data: postData,
            dataType: 'json',
            error: function () {
                alert('Error al iniciar sesion');
            },
            success: function (resp) {
                window.location.href = resp.redirect;
            }
        });
    });


    $('#form_register').on('submit', function () {
        var postData = $(this).serialize();
        var formURL = $(this).attr("action");
        console.log(postData+formURL);
        $.ajax({
            url: formURL,
            type: 'post',
            data: postData,
            dataType: 'json',
            error: function () {
                alert('Error de registro');
            },
            success: function (resp) {
                window.location.href = resp.redirect;
            }
        });
    });

    //-----------------
    $("#input1").hide();
    $("#input2").hide();
    $("#input3").hide();
    $("#input4").hide();
    $("#input5").hide();
    $("#input6").hide();
    $("#input7").hide();
    $("#input8").hide();
    $("#input9").hide();
    
    $("#li1").hide();
    $("#li2").hide();
    $("#li3").hide();
    $("#li4").hide();
    $("#li5").hide();
    $("#li6").hide();
    $("#li7").hide();
    $("#li8").hide();

    $("#tipo").change(function(){
        //alert("Hola");
        var x=$(this).val();
        //alert(x);
        if(x==2){
                $("#input1").hide();
                $("#input2").hide();
                $("#input3").hide();
                $("#input4").hide();
                $("#input5").hide();
                $("#input6").hide();
                $("#input7").hide();
                $("#input8").hide();
                //-----------------
                $("#li1").hide();
                $("#li2").hide();
                $("#li3").hide();
                $("#li4").hide();
                $("#li5").hide();
                $("#li6").hide();
                $("#li7").hide();
                $("#li8").hide();

            $(".gen").show();
        }
        else if(x==""){
                $("#input1").hide();
                $("#input2").hide();
                $("#input3").hide();
                $("#input4").hide();
                $("#input5").hide();
                $("#input6").hide();
                $("#input7").hide();
                $("#input8").hide();
                $("#input9").hide();
                //----------------
                $("#li1").hide();
                $("#li2").hide();
                $("#li3").hide();
                $("#li4").hide();
                $("#li5").hide();
                $("#li6").hide();
                $("#li7").hide();
                $("#li8").hide();
        }   
        else{
                $("#input1").hide();
                $("#input2").hide();
                $("#input3").hide();
                $("#input4").hide();
                $("#input5").hide();
                $("#input6").hide();
                $("#input7").hide();
                $("#input8").hide();
                $("#input9").hide();
                //-----------------
                $("#li1").hide();
                $("#li2").hide();
                $("#li3").hide();
                $("#li4").hide();
                $("#li5").hide();
                $("#li6").hide();
                $("#li7").hide();
                $("#li8").hide();

                $("#input1").show();
                $("#input2").show();
                $("#input3").show();
                $("#input4").show();
                $("#input5").show();
                $("#input6").show();
                $("#input7").show();
                $("#input9").show();
                
                //-----------------
                $("#li1").show();
                $("#li2").show();
                $("#li3").show();
                $("#li4").show();
                $("#li5").show();
                $("#li6").show();
                $("#li7").show();
                
        }
    });

    //Redireccionar desde el error hasta el home
    $("#gohome").click(function(){
        //alert("siiii");
        $.ajax({
            url:'/Contents/error/redireccionar',
            dataType:'json',
            success:function(x){
                 window.location.href = x.redirect;
            }
        })
    });

    //En este apartado sabremos cuando estamos en el dashboard para ver la tabla de usuarios directamente
    var localiz=window.location.href;
    //alert(localiz);
    var local_aux=localiz.split("/");
    //alert(local_aux);
    var longit=local_aux.length;
    //alert(longit);
    if(local_aux[longit-1]=="dashboard")
    {
      //alert("Estoy en el dashboard, con lo que me tocaria mostarar la tabla de usuarios");
      mostrarTablaUsuarios();
      mostrarTablaCuentos();
    }

    function mostrarTablaUsuarios(){
        alert("Estoy en el dashboard, con lo que me tocaria mostarar la tabla de usuarios");
    }
    function mostrarTablaCuentos(){
        alert("Estoy en el dashboard, con lo que me tocaria mostarar la tabla de cuentos")
    }


    //Función para registrar usuarios desde el dashboard
    $("form.nu").on("submit", function(){
        //alert("Vamos a registrar algo");


        //Esta linea de abajo ayuda a que el ajax no reemplace el documento html a lo absurdo con lo que recibe de php
        event.preventDefault();
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////

        var formURL=$(this).attr("action");
        var postData=$(this).serialize();
        alert(formURL);
        alert(postData);
        $.ajax({
            url:formURL,
            method:'POST',
            data:postData,
            dataType:'json',
            success:function(output)
            {
                if(output==1)
                {
                    alert("Registrado correctamente");
                }
                else
                {
                    alert("Esto tiene sentido?");
                }
            }
        })
    });
}); 
